#!/bin/bash

mkdir agent;
# mkdir client;

# create CA
openssl genrsa -out ca.key 2048
openssl req -x509 -new -key ca.key -days 10000 -out ca.crt

# create agent sert
openssl genrsa -out agent/agent.key 2048
openssl req -new -key agent/agent.key -out agent/agent.csr

# sign
openssl x509 -req -in agent/agent.csr -CA ca.crt -CAkey ca.key -CAcreateserial -out agent/agent.crt -days 5000

# create client sert
#openssl genrsa -out client/client.key 2048
#openssl req -new -key client/client.key -out client/client.csr

# sign
#openssl x509 -req -in client/client.csr -CA ca.crt -CAkey ca.key -CAserial ca.srl -out client/client.crt -days 5000

cat ca.key ca.crt > ca.pem
