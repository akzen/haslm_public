from argparse import ArgumentParser
import logging
from haslm import db, server, controller
import urllib3


def main():
    urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
    parser = ArgumentParser(description='Backend service of HASLM project')
    parser.add_argument('--create-tables', type=str, dest='default_user_login',
                        help='Creates data base tables before launch.')
    parser.add_argument('role', metavar='role', type=str,
                        help='A role of application instance: server, monitor')
    args = parser.parse_args()
    if args.default_user_login:
        db.create_tables(args.default_user_login)
    if args.role == 'server':
        logging.info('Starting server')
        server.run()
    elif args.role == 'controller':
        logging.info('Starting controller')
        controller.run()
    else:
        logging.critical('Unknown role, exit...')


if __name__ == '__main__':
    main()
