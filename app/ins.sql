update script set body = 'import os
import pwd


def user_exist(login):
    try:
        pwd.getpwnam(login)
        return True
    except KeyError:
        return False


def manage_rights(login, path, right):
    os.system("chown " + login + ":" + login + " " + path)
    os.system("chmod " + str(right) + " " + path)


def main():
    logins = os.getenv("LOGINS").split(\ \)
    key_data = os.getenv("KEYS_DATA").split(\^^^^^^^^^^\\n\)

    for i in range(len(logins)):
        if not user_exist(logins[i]):
            os.system("useradd " + logins[i] + " -m -s /bin/bash")

        user_ssh_path = "/home/" + logins[i] + "/.ssh"
        if not os.path.exists(user_ssh_path):
            os.system("mkdir " + user_ssh_path)
            manage_rights(logins[i], user_ssh_path, 700)

        with open(user_ssh_path + \'/autorized_keys\', \'w\') as f:
            for line in key_data[i].split(\\\\n\'):
                f.write(line)
                f.write(\\\n\)
        manage_rights(logins[i], user_ssh_path + \/autorized_keys\, 600)


if __name__ == \'__main__\':
    main()'
where name = update_keys;
