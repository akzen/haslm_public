from .. import cfg
from ..server.exceptions import NotJsonError, NoData
from ..server import executor
from ..db import *
from .keys_sender import *
from .check_machines import *
from .users_cleaner import *
import time
from datetime import datetime, timedelta
import logging


def save_logs(action, description):
    with get_session() as s:
        act = Logs(executor="controller", action=action,
                   description=description)
        s.add(act)


def run():
    last_send = datetime.utcnow()
    last_check = datetime.utcnow()
    last_clean = datetime.utcnow()

    while True:
        try:
            now_time = datetime.utcnow()
            if now_time - last_send >= timedelta(seconds=cfg.SEND_INTERVAL):
                send_keys()
                last_send = now_time

        except Exception as e:
            logging.warning(e)
            save_logs('sync', 'Syncing failed ' + str(e))

        try:
            if now_time - last_check >= timedelta(seconds=cfg.CHECK_INTERVAL):
                check()
                print("check")
                last_check = now_time

        except Exception as e:
            logging.warning(e)
            save_logs('check', 'Checking failed ' + str(e))

        try:
            if now_time - last_clean >= timedelta(seconds=cfg.CLEAN_INTERVAL):
                clean_users()
                last_clean = now_time

        except Exception as e:
            logging.warning(e)
            save_logs('clean', 'Cleaning users failed ' + str(e))

        time.sleep(1)
