from ..db import *
from ..server.exceptions import NotJsonError, NoData
from sqlalchemy.exc import IntegrityError
from ..server import executor


def clean_users():
    machines = get_machines()

    for machine in machines:
        users = list_users_on_machine(machine.id)

        if not users:
            continue

        user_list = ""
        user_delim = ""
        for user in users:
            user_login = user.login.replace('.', '_').replace('@', '_')
            user_list += user_delim + user_login
            user_delim = " "

        print("delete users")
        print(machine.id)
        print(user_list)
        env = {"LOGINS": user_list}
        executor.execute([machine], get_script('clean_users'), env=env)


def get_machines():
    with get_session() as s:
        return s.query(Machine).filter(Machine.status == 'active').all()


def list_users_on_machine(machine):
    result = set()
    with get_session() as s:
        tmp = s.query(User, Access).filter(
            Access.machine_id == machine,
            Access.status == 'deleted',
            Access.on_machine == True,
            User.id == Access.vmuser_id,
        ).all()
        for user, access in tmp:
            result.add(user)
            access.on_machine = False
    return result


def get_script(script):
    with get_session() as s:
        result = s.query(Script).filter(
            Script.name == script
        ).first()
    return result
