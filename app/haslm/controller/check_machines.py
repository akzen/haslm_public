from ..db import *
from ..server.exceptions import NotJsonError, NoData
from sqlalchemy.exc import IntegrityError
from ..server import executor
import smtplib
from email.message import EmailMessage
import requests
import os


def send_email(host):
    with get_session() as s:
        admins = s.query(User).filter(
            User.admin == True,
            User.status == 'active'
        ).all()

        server = smtplib.SMTP_SSL('smtp.mail.ru', 465)
        server.login("biv154@mail.ru", "sidf7t84w5jngjks")
        
        for admin in admins:
            server.sendmail(
                "biv154@mail.ru",
                admin.login,
                "Troubles on " + host
            )
        server.quit()


def check():
    with get_session() as s:
        machines = s.query(Machine).filter(Machine.status == 'active').all()
        for machine in machines:
            url = 'https://{}:{}/status'.format(machine.address,
                                                cfg.AGENT_PORT)
            try:
                r = requests.get(url, cert=cfg.CERT, verify=False).json()
                sshd_pid = r["sshd_pid"]
                agent_pid = r["agent_pid"]
                print(str(sshd_pid) + "------" + str(machine.sshd_pid))
                print(str(agent_pid) + "------" + str(machine.agent_pid))
                if (machine.sshd_pid is not None and
                        (machine.sshd_pid != sshd_pid or
                            machine.agent_pid is not None) and
                        machine.agent_pid != agent_pid):
                    send_email(machine.address)
                    print('send email for' + str(machine.address))
                machine.sshd_pid = sshd_pid
                machine.agent_pid = agent_pid
            except requests.exceptions.RequestException as e:
                send_email(machine.address)
