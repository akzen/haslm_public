from ..db import *
from ..server.exceptions import NotJsonError, NoData
from sqlalchemy.exc import IntegrityError
from ..server import executor


def send_keys():
    machines = get_machines()

    for machine in machines:
        users = list_users_on_machine(machine.id)

        if not users:
            continue

        user_list = ""
        keys_list = ""
        user_separator = ""
        keys_separator = ""
        for user in users:
            user_login = user.login.replace('.', '_').replace('@', '_')
            user_list += user_separator + user_login
            user_separator = " "
            keys = user_keys(user.id)
            keys_user = ""
            for key in keys:
                keys_user += key
                keys_user += '\n'
            keys_list += keys_separator + keys_user
            keys_separator = '^^^^^^^^^^\n'
        print("send keys")
        print(machine.id)
        print(user_list)
        print(keys_list)
        env = {"LOGINS": user_list, "KEYS_DATA": keys_list}
        executor.execute([machine], get_script('update_keys'), env=env)


def get_machines():
    with get_session() as s:
        return s.query(Machine).filter(Machine.status == 'active').all()


def get_script(script):
    with get_session() as s:
        result = s.query(Script).filter(
            Script.name == script
        ).first()
    return result


def list_users_on_machine(machine):
    result = set()
    with get_session() as s:
        tmp = s.query(User, Access).filter(
            Access.machine_id == machine,
            Access.status == 'active',
            User.id == Access.vmuser_id,
            User.status == 'active'
        ).all()
        for user, _ in tmp:
            result.add(user)
    return result


def user_keys(user_id):
    result = set()
    with get_session() as s:
        keys = s.query(User, PublicKey).filter(
            User.id == user_id,
            User.status == 'active',
            PublicKey.vmuser_id == User.id,
            PublicKey.status == 'active'
        ).all()
        for _, key in keys:
            result.add(key.body)
    return result
