from sqlalchemy import (Column, Integer, String, ForeignKey,
                        DateTime, Boolean, UniqueConstraint)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import ENUM, UUID
from flask_login import UserMixin

from datetime import datetime
import uuid


Base = declarative_base()


Status = ENUM('active', 'deleted', name='status')

Action = ENUM('add access', 'remove access', 'sync',
              'check', 'clean', 'register', 'create',
              'delete', 'run sctipt', 'add ssh-key',
              'remove ssh-key', name='action')


class User(Base, UserMixin):
    __tablename__ = 'vmuser'

    id = Column(Integer, primary_key=True)
    login = Column(String, unique=True, nullable=False)
    name = Column(String, nullable=False)
    surname = Column(String, nullable=False)
    cookie_id = Column(UUID(as_uuid=True), default=uuid.uuid4,
                       unique=True, nullable=False)
    admin = Column(Boolean, default=False, nullable=False)
    status = Column(Status, default='active', nullable=False)
    logins = Column(Integer, default=0)

    def get_id(self):
        return self.cookie_id


class Machine(Base):
    __tablename__ = 'machine'

    id = Column(Integer, primary_key=True)
    address = Column(String, unique=True, nullable=False)
    domain = Column(String)
    status = Column(Status, default='active', nullable=False)
    sshd_pid = Column(Integer)
    agent_pid = Column(Integer)


class Access(Base):
    __tablename__ = 'vmuser_to_machine_access'

    id = Column(Integer, primary_key=True)
    vmuser_id = Column(Integer, ForeignKey('vmuser.id'), nullable=False)
    machine_id = Column(Integer, ForeignKey('machine.id'), nullable=False)
    issued = Column(DateTime, default=datetime.utcnow, nullable=False)
    disabled = Column(DateTime)
    status = Column(Status, default='active', nullable=False)
    on_machine = Column(Boolean, default=True, nullable=False)


class PublicKey(Base):
    __tablename__ = 'public_keys'

    id = Column(Integer, primary_key=True)
    status = Column(Status, default='active', nullable=False)
    vmuser_id = Column(Integer, ForeignKey('vmuser.id'), nullable=False)
    body = Column(String, unique=True, nullable=False)
    description = Column(String, nullable=False)
    update_time = Column(DateTime, default=datetime.utcnow, nullable=False)


class Token(Base):
    __tablename__ = 'token'

    id = Column(UUID(as_uuid=True), default=uuid.uuid4, primary_key=True)
    vmuser_id = Column(Integer, ForeignKey('vmuser.id'), nullable=False)
    issued = Column(DateTime, default=datetime.utcnow, nullable=False)
    status = Column(Status, default='active', nullable=False)


class Script(Base):
    __tablename__ = 'script'

    name = Column(String, primary_key=True)
    shell = Column(String, default='bash', nullable=False)
    body = Column(String, nullable=False)


class ExecutorLog(Base):
    __tablename__ = 'executer_log'

    id = Column(Integer, primary_key=True)
    execute_id = Column(UUID(as_uuid=True), nullable=False)
    machine_id = Column(Integer, ForeignKey('machine.id'))
    script = Column(String)
    env = Column(String)
    code = Column(Integer)
    stdout = Column(String)
    stderr = Column(String)


class Logs(Base):
    __tablename__ = 'activity_logs'

    id = Column(Integer, primary_key=True)
    executor = Column(String)
    action = Column(Action, nullable=False)
    description = Column(String, nullable=False)
    update_time = Column(DateTime, default=datetime.utcnow, nullable=False)
