from .. import cfg
from .models import *
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from contextlib import contextmanager
import logging


_engine = create_engine(cfg.DB_CONNECTION_STRING)
_Session = sessionmaker(bind=_engine, expire_on_commit=False)


@contextmanager
def get_session():
    session = _Session()
    try:
        yield session
        session.commit()
    except:
        session.rollback()
        raise
    finally:
        session.close()


def create_tables(default_user_login):
    logging.info('Dropping existing tables')
    for tbl in reversed(Base.metadata.sorted_tables):
        try:
            tbl.drop(_engine)
        except:
            pass
    logging.info('Creating tables')
    Base.metadata.create_all(_engine)
    logging.info('Tables was created')
    with get_session() as s:
        root = User(
            login=default_user_login,
            name='default',
            surname='default',
            admin=True,
        )
        s.add(root)
        for name, body in SCRIPTS.items():
            s.add(Script(name=name, body=body))
        for name, body in PY_SCRIPTS.items():
            s.add(Script(name=name, shell='python3', body=body))
    logging.info('Default user [default_user_login] was created')


def upgrade_schema():
    logging.info('Upgrading tables schema')
    from .models import Tokens as tb
    tb.__table__.create(_engine)


PY_SCRIPTS = {
    'update_keys': '''
import os
import pwd


def user_exist(login):
    try:
        pwd.getpwnam(login)
        return True
    except KeyError:
        return False


def manage_rights(login, path, right):
    os.system("chown " + login + ":" + login + " " + path)
    os.system("chmod " + str(right) + " " + path)


def main():
    logins = os.getenv("LOGINS").split(' ')
    key_data = os.getenv("KEYS_DATA").split('^^^^^^^^^^\\n')

    for i in range(len(logins)):
        if not user_exist(logins[i]):
            os.system("useradd " + logins[i] + " -m -s /bin/bash")

        user_ssh_path = "/home/" + logins[i] + "/.ssh"
        if not os.path.exists(user_ssh_path):
            os.system("mkdir " + user_ssh_path)
            manage_rights(logins[i], user_ssh_path, 700)

        with open(user_ssh_path + '/authorized_keys', 'w') as f:
            for line in key_data[i].split('\\\\n'):
                f.write(line)
                f.write('\\n')
        manage_rights(logins[i], user_ssh_path + '/authorized_keys', 600)


if __name__ == '__main__':
    main()
''',
    'clean_users': '''
import os
import pwd


def user_exist(login):
    try:
        pwd.getpwnam(login)
        return True
    except KeyError:
        return False


def main():
    logins = os.getenv("LOGINS").split(' ')

    for login in logins:
        if user_exist(login):
            os.system("pkill -u " + login)
            os.system("deluser " + login +
                      " --remove-home --backup --backup-to /tmp")


if __name__ == '__main__':
    main()
'''
}


SCRIPTS = {
    'add_key': '''
        #!/bin/bash

        sudo -u $LOGIN bash << EOF
        cd /home/$LOGIN &&
        echo -e "$KEY_DATA" >> .ssh/autorized_keys
        EOF
    ''',
    'add_keys': '''
        #!/bin/bash

        for login in $LOGINS
        do
            sudo -u $login bash << EOF
            cd /home/$login/.ssh/ &&
            rm -f autorized_keys &&
            echo -e "$KEY_DATA" >> autorized_keys
            EOF
        done
    ''',
    'add_user': '''
        #!/bin/bash

        useradd $LOGIN -m -s /bin/bash &&
        sudo -u $LOGIN bash << EOF
        cd /home/$LOGIN
        mkdir .ssh
        chmod 700 .ssh
        echo -e "$KEY_DATA" > .ssh/autorized_keys
        chmod 600 .ssh/autorized_keys
        EOF
    ''',
    'del_key': '''
        #!/bin/bash


        sudo -u $LOGIN bash << EOF
        cd /home/$LOGIN/.ssh &&
        cp autorized_keys autorized_keys_old &&
        cat autorized_keys_old | grep -v -E "($KEY_IDS)" > autorized_keys &&
        rm -f autorized_keys_old
        EOF
    ''',
    'del_user': '''
        #!/bin/bash

        pkill -u $LOGIN;
        deluser $LOGIN --remove-home --backup --backup-to /tmp
    ''',
    'set_password': '''
        #!/bin/bash
        echo -e "$PASSWORD\n$PASSWORD" | passwd $LOGIN
    ''',
}
