from flask_wtf import FlaskForm
from wtforms import (BooleanField, StringField, PasswordField, validators,
                     TextAreaField, RadioField, IntegerField)


class AuthForm(FlaskForm):
    email = StringField('Email Address', [validators.DataRequired()])
    password = PasswordField('Password')


class FeedbackForm(FlaskForm):
    email = StringField('Email Address', [validators.DataRequired()])
    text = TextAreaField()


class RegisterForm(FlaskForm):
    login = StringField('Login', [validators.DataRequired()])
    name = StringField('Name', [validators.DataRequired()])
    surname = StringField('Surname', [validators.DataRequired()])
    role = BooleanField('Role')


class RemoveForm(FlaskForm):
    login = StringField('Login', [validators.DataRequired()])


class AccessForm(FlaskForm):
    login = StringField('Login', [validators.DataRequired()])
    host = StringField('Host Address', [validators.DataRequired()])


class GrantFormMachine(FlaskForm):
    login = StringField('Login', [validators.DataRequired()])


class GrantFormUser(FlaskForm):
    host = StringField('Host Address', [validators.DataRequired()])


class AddMachineForm(FlaskForm):
    domain = StringField('Domain', [validators.DataRequired()])
    host = StringField('Host Address', [validators.DataRequired()])


class DeleteMachineForm(FlaskForm):
    host = StringField('Host Address', [validators.DataRequired()])


class UploadForm(FlaskForm):
    name = StringField('Key name', [validators.DataRequired()])
    text = TextAreaField('Key data', [validators.DataRequired()])
