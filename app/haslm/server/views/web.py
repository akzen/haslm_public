from flask import (Blueprint, request, redirect, url_for,
                   render_template, jsonify, abort)
from flask_login import (login_required, login_user, logout_user,
                         login_fresh, current_user)
from .. import auth
from .. import forms
from .. import api
import logging


mod = Blueprint('general', __name__)


@mod.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('general.machines'))
    form = forms.AuthForm(request.form)
    message = ''
    if request.method == "POST" and form.validate_on_submit():
        user = auth.dummy_auth(form.email.data, form.password.data)
        if user:
            login_user(user)
            api.update_logins(form.email.data)
            return redirect(url_for('general.machines'))
        else:
            message = 'Invalid user'
    return render_template('login.html', form=form, message=message)


@mod.route('/logout', methods=['GET', 'POST'])
@login_required
def logout():
    logout_user()
    return redirect(url_for('general.login'))


@mod.route('/blank')
@login_required
def blank():
    return render_template('blank.html', current_user=current_user)


@mod.route('/manage_users')
@login_required
def manage_users():
    if current_user.admin:
        form_register = forms.RegisterForm(request.form)
        form_remove = forms.RemoveForm(request.form)
        message_register = ''
        message_remove = ''
        
        return render_template(
            'manage_users.html',
            current_user=current_user,
            form_register=form_register,
            form_remove=form_remove,
            message_register=message_register,
            message_remove=message_remove,
        )
    else:
        abort(404)


@mod.route('/register', methods=['POST'])
@login_required
def register_usr():
    if current_user.admin:
        form_register = forms.RegisterForm(request.form)
        form_remove = forms.RemoveForm(request.form)
        message_register = ''
        message_remove = ''
        if form_register.validate_on_submit():
            try:
                api.register_user(current_user.login,
                                  form_register.login.data,
                                  form_register.name.data,
                                  form_register.surname.data,
                                  form_register.role.data)
                message_register = f'{form_register.login.data} was registered'
                form_register.login.data = ''
                form_register.name.data = ''
                form_register.surname.data = ''
                form_register.role.data = ''
                form_remove.login.data = ''
            except Exception as e:
                message_register = 'Failed to register user, probably one already exists.\n{}'.format(str(e))
        return render_template(
            'manage_users.html',
            current_user=current_user,
            form_register=form_register,
            form_remove=form_remove,
            message_register=message_register,
            message_remove=message_remove,
        )
    else:
        abort(404)


@mod.route('/remove', methods=['POST'])
@login_required
def remove_usr():
    if current_user.admin:
        form_register = forms.RegisterForm(request.form)
        form_remove = forms.RemoveForm(request.form)
        message_register = ''
        message_remove = ''
        if form_remove.validate_on_submit():
            try:
                api.delete_user(current_user.login, form_remove.login.data)
                message_remove = f'{form_remove.login.data} was deleted'
                form_register.login.data = ''
                form_register.name.data = ''
                form_register.surname.data = ''
                form_register.role.data = ''
                form_remove.login.data = ''
            except Exception as e:
                message_remove = 'Failed to delete user, probably it has been deleted yet.\n{}'.format(str(e))
        return render_template(
            'manage_users.html',
            current_user=current_user,
            form_register=form_register,
            form_remove=form_remove,
            message_register=message_register,
            message_remove=message_remove,
        )
    else:
        abort(404)


@mod.route('/')
@mod.route('/machines')
@login_required
def machines():
    if current_user.admin:
        machines = api.get_all_machines()
        return render_template(
            'machines_admin.html',
            current_user=current_user,
            machines=machines
        )
    else:
        machines = api.get_users_machines(current_user.login)
        return render_template(
            'machines_user.html',
            current_user=current_user,
            machines=machines
        )


@mod.route('/users')
@login_required
def users_list():
    if current_user.admin:
        usrs = api.get_all_users()
        return render_template(
            'users.html',
            current_user=current_user,
            usrs=usrs
        )
    else:
        abort(404)


@mod.route('/instructions')
@login_required
def instructions():
    if not current_user.admin:
        return render_template('instructions.html', current_user=current_user)
    else:
        abort(404)


@mod.route('/manage_machines')
@login_required
def manage_machines():
    if current_user.admin:
        form_add = forms.AddMachineForm(request.form)
        form_delete = forms.DeleteMachineForm(request.form)
        message_add = ''
        message_delete = ''
        
        return render_template(
            'manage_machines.html',
            current_user=current_user,
            form_add=form_add,
            form_delete=form_delete,
            message_add=message_add,
            message_delete=message_delete,
        )
    else:
        abort(404)


@mod.route('/add_machine', methods=['POST'])
@login_required
def add_machine():
    if current_user.admin:
        form_add = forms.AddMachineForm(request.form)
        form_delete = forms.DeleteMachineForm(request.form)
        message_add = ''
        message_delete = ''
        if form_add.validate_on_submit():
            try:
                api.new_machine(current_user.login, form_add.host.data,
                                form_add.domain.data)
                message_add = f'Machine {form_add.domain.data} was created'
                form_add.host.data = ''
                form_add.domain.data = ''
                form_delete.host.data = ''
            except Exception as e:
                message_add = 'Failed to create machine, probably one already exists.\n{}'.format(str(e))
        return render_template(
            'manage_machines.html',
            current_user=current_user,
            form_add=form_add,
            form_delete=form_delete,
            message_add=message_add,
            message_delete=message_delete,
        )
    else:
        abort(404)


@mod.route('/remove_machine', methods=['POST'])
@login_required
def remove_machine():
    if current_user.admin:
        form_add = forms.AddMachineForm(request.form)
        form_delete = forms.DeleteMachineForm(request.form)
        message_add = ''
        message_delete = ''
        if form_delete.validate_on_submit():
            try:
                api.delete_machine(current_user.login, form_delete.host.data)
                message_delete = f'Machine {form_delete.host.data} was deleted'
                form_add.host.data = ''
                form_add.domain.data = ''
                form_delete.host.data = ''
            except Exception as e:
                message_delete = 'Failed to delete machine, probably it has been deleted yet.\n{}'.format(str(e))
        return render_template(
            'manage_machines.html',
            current_user=current_user,
            form_add=form_add,
            form_delete=form_delete,
            message_add=message_add,
            message_delete=message_delete,
        )
    else:
        abort(404)


@mod.route('/settings', methods=['GET', 'POST'])
@login_required
def settings():
    form = forms.UploadForm(request.form)
    message = ''
    if request.method == "POST" and form.validate_on_submit():
        try:
            api.upload_public_key(current_user.login, form.text.data,
                                  form.name.data)
            message = 'Upload success!'
            form.name.data = ''
            form.text.data = ''
        except Exception as e:
            message_delete = 'Failed to add key, probably one already exists.(\n{})'.format(str(e))

    keys = api.user_keys(current_user.login)

    if current_user.admin:
        token = auth.get_token(current_user)
        return render_template(
            'settings_admin.html',
            form=form,
            current_user=current_user,
            message=message,
            keys=keys,
            token=token
        )
    else:
        return render_template(
            'settings_user.html',
            form=form,
            current_user=current_user,
            message=message,
            keys=keys
        )


@mod.route('/user/<string:name>', methods=['GET', 'POST'])
@login_required
def user_name(name):
    if api.user_exist(name):
        if current_user.admin:
            form = forms.GrantFormUser(request.form)
            message = ''
            if request.method == "POST" and form.validate_on_submit():
                try:
                    hosts = form.host.data.replace(' ', '').split(',')
                    api.grant_access(current_user.login, [name], hosts)
                    message = 'Access was granted'
                    form.host.data = ''
                except Exception as e:
                    message = 'Failed to grant user access:\n{}'.format(str(e))

            keys = api.user_keys(name)
            machines = api.get_users_machines(name)
            return render_template(
                'user_page.html',
                form=form,
                title=name,
                current_user=current_user,
                machines=machines,
                keys=keys,
                message=message
            )
    else:
        abort(404)


@mod.route('/machine/<string:address>', methods=['GET', 'POST'])
@login_required
def machine_name(address):
    if api.machine_exist(address):
        if current_user.admin:
            form = forms.GrantFormMachine(request.form)
            message = ''
            if request.method == "POST" and form.validate_on_submit():
                try:
                    logins = form.login.data.replace(' ', '').split(',')
                    api.grant_access(current_user.login, logins, [address])
                    message = 'Access was granted'
                    form.login.data = ''
                except Exception as e:
                    message = 'Failed to grant user access:\n{}'.format(str(e))

            usrs = api.get_users_on_machine(address)
            return render_template(
                'machine_page.html',
                form=form,
                current_user=current_user,
                usrs=usrs,
                title_domain=api.host_name(address),
                title_address=address,
                message=message
            )
    else:
        abort(404)


@mod.route('/activity')
@login_required
def activity():
    if current_user.admin:
        logs = api.get_logs()
        return render_template(
            'activity.html',
            current_user=current_user,
            logs=logs
        )
    else:
        abort(404)


@mod.route('/manage_accesses')
@login_required
def manage_accesses():
    if current_user.admin:
        grant_form = forms.AccessForm(request.form)
        revoke_form = forms.AccessForm(request.form)
        message_grant = ''
        message_revoke = ''
        
        return render_template(
            'manage_accesses.html',
            current_user=current_user,
            grant_form=grant_form,
            revoke_form=revoke_form,
            message_grant=message_grant,
            message_revoke=message_revoke
        )
    else:
        abort(404)


@mod.route('/grant', methods=['POST'])
@login_required
def grant():
    if current_user.admin:
        grant_form = forms.AccessForm(request.form)
        revoke_form = forms.AccessForm(request.form)
        message_grant = ''
        message_revoke = ''
        if grant_form.validate_on_submit():
            try:
                logins = grant_form.login.data.replace(' ', '').split(',')
                hosts = grant_form.host.data.replace(' ', '').split(',')
                api.grant_access(current_user.login, logins, hosts)
                message_grant = 'Access was granted'
                grant_form.login.data = ''
                grant_form.host.data = ''
                revoke_form.login.data = ''
                revoke_form.host.data = ''
            except Exception as e:
                message_grant = f'Failed to grant user access:\n{str(e)}'

        return render_template(
            'manage_accesses.html',
            current_user=current_user,
            grant_form=grant_form,
            revoke_form=revoke_form,
            message_grant=message_grant,
            message_revoke=message_revoke
        )
    else:
        abort(404)


@mod.route('/revoke', methods=['POST'])
@login_required
def revoke():
    if current_user.admin:
        grant_form = forms.AccessForm(request.form)
        revoke_form = forms.AccessForm(request.form)
        message_grant = ''
        message_revoke = ''
        if revoke_form.validate_on_submit():
            try:
                logins = revoke_form.login.data.replace(' ', '').split(',')
                hosts = revoke_form.host.data.replace(' ', '').split(',')
                api.revoke_access(current_user.login, logins, hosts)
                message_revoke = 'Access was revoked'
                grant_form.login.data = ''
                grant_form.host.data = ''
                revoke_form.login.data = ''
                revoke_form.host.data = ''
            except Exception as e:
                message_revoke = f'Failed to revoke user access:\n{str(e)}'

        return render_template(
            'manage_accesses.html',
            current_user=current_user,
            grant_form=grant_form,
            revoke_form=revoke_form,
            message_grant=message_grant,
            message_revoke=message_revoke
        )
    else:
        abort(404)


@mod.route('/wt/<string:address>')
@login_required
def web_terminal(address):
    if api.machine_exist(address):
        if current_user.admin:
            return render_template(
                'web_terminal.html',
                current_user=current_user,
                title=api.host_name(address) + '(' + address + ')',
                address=address,
            )
        else:
            if api.access_exist(current_user.login, address):
                return render_template(
                    'web_terminal.html',
                    current_user=current_user,
                    title=api.host_name(address) + '(' + address + ')',
                    address=address,
                )
            else:
                abort(404)
    else:
        abort(404)


@mod.route('/tests')
@login_required
def tests():
    if current_user.admin:
        form_add = forms.AddMachineForm(request.form)
        form_delete = forms.DeleteMachineForm(request.form)
        message_add = ''
        message_delete = ''
        
        return render_template(
            'test.html',
            current_user=current_user,
            form_add=form_add,
            form_delete=form_delete,
            message_add=message_add,
            message_delete=message_delete,
        )
    else:
        abort(404)


# Тестовые ручки для тестовых страниц с джава скриптом


@mod.route('/_manage_machines')
@login_required
def _manage_machines():
    if current_user.admin:
        form_add = forms.AddMachineForm(request.form)
        form_delete = forms.DeleteMachineForm(request.form)
        
        return render_template(
            '_manage_machines.html',
            current_user=current_user,
            form_add=form_add,
            form_delete=form_delete
        )
    else:
        abort(404)


@mod.route('/_manage_users')
@login_required
def _manage_users():
    if current_user.admin:
        form_register = forms.RegisterForm(request.form)
        form_remove = forms.RemoveForm(request.form)
        
        return render_template(
            '_manage_users.html',
            current_user=current_user,
            form_register=form_register,
            form_remove=form_remove
        )
    else:
        abort(404)


@mod.route('/_manage_accesses')
@login_required
def _manage_accesses():
    if current_user.admin:
        grant_form = forms.AccessForm(request.form)
        revoke_form = forms.AccessForm(request.form)
        
        return render_template(
            '_manage_accesses.html',
            current_user=current_user,
            grant_form=grant_form,
            revoke_form=revoke_form
        )
    else:
        abort(404)


@mod.route('/_machine/<string:address>')
@login_required
def _machine_name(address):
    if api.machine_exist(address):
        if current_user.admin:
            form = forms.GrantFormMachine(request.form)
            usrs = api.get_users_on_machine(address)

            return render_template(
                '_page_machine.html',
                form=form,
                current_user=current_user,
                usrs=usrs,
                title_domain=api.host_name(address),
                title_address=address
            )
    else:
        abort(404)


@mod.route('/_user/<string:name>')
@login_required
def _user_name(name):
    if api.user_exist(name):
        if current_user.admin:
            form = forms.GrantFormUser(request.form)
            keys = api.user_keys(name)
            machines = api.get_users_machines(name)

            return render_template(
                '_page_user.html',
                form=form,
                title=name,
                current_user=current_user,
                machines=machines,
                keys=keys
            )
    else:
        abort(404)


@mod.route('/_settings')
@login_required
def _settings():
    form = forms.UploadForm(request.form)
    keys = api.user_keys(current_user.login)

    if current_user.admin:
        token = auth.get_token(current_user)
        return render_template(
            '_settings_admin.html',
            form=form,
            current_user=current_user,
            keys=keys,
            token=token
        )
    else:
        return render_template(
            '_settings_user.html',
            form=form,
            current_user=current_user,
            keys=keys
        )


def page_not_found(e):
    return render_template(
        '404.html',
        login=current_user.login
        ), 404
