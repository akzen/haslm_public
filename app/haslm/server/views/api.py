from .. import api
from .. import auth
from ..exceptions import NotJsonError, NoData
from ...controller import keys_sender, check_machines

from sqlalchemy.exc import IntegrityError
from flask import Blueprint, jsonify, request, make_response
from flask_login import login_required, current_user


mod = Blueprint('api', __name__, url_prefix='/api')


def make_400(text='Invalid reqeust'):
    body = jsonify(error=text)
    return make_response(body, 400)


def make_ok(description=None):
    body = {
        'status': 'ok',
    }
    if description:
        body['description'] = description
    return jsonify(body)


@mod.route('/unauthorized', methods=['GET'])
def unauthorized():
    body = jsonify(error='Unauthorized')
    return make_response(body, 401)


@mod.route('/issue_token', methods=['GET'])
@login_required
def issue_token():
    if current_user.admin:
        return jsonify(token=auth.issue_token(current_user))
    else:
        return make_400('You dont have administrator rights')



@mod.route('/token', methods=['GET'])
@login_required
def token():
    if current_user.admin:
        return jsonify(token=auth.get_token(current_user))
    else:
        return make_400('You dont have administrator rights')



@mod.route('/register_user', methods=['POST'])
@login_required
def register_user():
    if current_user.admin:
        try:
            args = request.get_json()
            if not args:
                return make_400('Expected json')
            is_admin = False
            if args['admin'] == "True":
                is_admin = True
            api.register_user(current_user.login, args['login'], args['name'],
                              args['surname'], is_admin)
            return make_ok('User was registered')
        except KeyError:
            return make_400()
        except IntegrityError:
            return make_400('User with this login already exists')
    else:
        return make_400('You dont have administrator rights')


@mod.route('/delete_user', methods=['POST'])
@login_required
def delete_user():
    if current_user.admin:
        try:
            args = request.get_json()
            if not args:
                return make_400('Expected json')
            api.delete_user(current_user.login, args['login'])
            return make_ok('User was deleted')
        except KeyError:
            return make_400()
        except NoData:
            return make_400('No such user')
    else:
        return make_400('You dont have administrator rights')


@mod.route('/create_machine', methods=['POST'])
@login_required
def create_machine():
    if current_user.admin:
        try:
            args = request.get_json()
            if not args:
                make_400('Expected json')
            api.new_machine(current_user.login,
                            args['address'], args['domain'])
            return make_ok('Machine was created')
        except KeyError:
            return make_400()
        except IntegrityError:
            return make_400('Machine with this address already exists')
    else:
        return make_400('You dont have administrator rights')


@mod.route('/remove_machine', methods=['POST'])
@login_required
def remove_machine():
    if current_user.admin:
        try:
            args = request.get_json()
            if not args:
                make_400('Expected json')
            api.delete_machine(current_user.login, args['address'])
            return make_ok('Machine was removed')
        except KeyError:
            return make_400()
        except NoData:
            return make_400('No such machine')
    else:
        return make_400('You dont have administrator rights')


@mod.route('/grant_access', methods=['POST'])
@login_required
def grant_access():
    if current_user.admin:
        try:
            args = request.get_json()
            if not args:
                make_400('Expected json')
            api.grant_access(current_user.login,
                             set(args['logins']), set(args['hosts']))
            return make_ok('Access was granted')
        except KeyError:
            return make_400()
        except NoData:
            return make_400("No such user or machine")
    else:
        return make_400('You dont have administrator rights')


@mod.route('/revoke_access', methods=['POST'])
@login_required
def revoke_access():
    if current_user.admin:
        try:
            args = request.get_json()
            if not args:
                make_400('Expected json')
            api.revoke_access(current_user.login,
                              set(args['logins']), set(args['hosts']))
            return make_ok('Access was revoked')
        except KeyError:
            return make_400()
        except NoData:
            return make_400("No such user or machine")
    else:
        return make_400('You dont have administrator rights')


@mod.route('/upload_public_key', methods=['POST'])
@login_required
def upload_public_key():
    try:
        args = request.get_json()
        if not args:
            return make_400('Expected json')
        api.upload_public_key(current_user.login,
                              args['key'], args['description'])
        return make_ok('Key uploaded')
    except KeyError:
        return make_400()
    except IntegrityError:
        return make_400('Key already in system')
    except NoData:
        return make_400('No such user')


@mod.route('/delete_public_key', methods=['POST'])
@login_required
def delete_public_key():
    try:
        args = request.get_json()
        if not args:
            return make_400('Expected json')
        if (not current_user.admin) and (current_user.login != args['login']):
            return make_400('Common users cant delete anothers keys')
        api.delete_public_key(current_user.login, args['login'],
                              int(args['key']))
        return make_ok('Key deleted')
    except KeyError:
        return make_400()
    except NoData:
        return make_400('No such key')


@mod.route('/user_machines', methods=['POST'])
@login_required
def user_machines():
    try:
        args = request.get_json()
        if not args:
            make_400('Expected json')
        if (not current_user.admin) and (current_user.login != args['login']):
            return make_400('Common users cant see anothers machines')
        return jsonify(api.get_users_machines(args['login']))
    except KeyError:
        return make_400()


@mod.route('/all_machines', methods=['GET'])
@login_required
def get_all_machines():
    if current_user.admin:
        return jsonify(api.get_all_machines())
    else:
        return make_400('You dont have administrator rights')


@mod.route('/all_users', methods=['GET'])
@login_required
def get_all_users():
    if current_user.admin:
        return jsonify(api.get_all_users())
    else:
        return make_400('You dont have administrator rights')


@mod.route('/web_terminal', methods=['POST'])
def web_terminal():
    try:
        args = request.get_json()
        if not args:
            make_400('Expected json')
        return jsonify(api.web_terminal(current_user.login, args['address']))
    except KeyError:
        return make_400()
    except NoData:
        return make_400("No such user or machine, or no access")


#@mod.route('/force_send_keys', methods=['GET'])
#@login_required
#def force_send_keys():
#    try:
#        keys_sender.send_keys()
#        return make_ok()
#    except Exception as e:
#        jsonify({'status': e})


@mod.route('/test_route', methods=['GET'])
@login_required
def test_route():
    try:
        return make_ok()
    except Exception as e:
        jsonify({'status': e})


@mod.route('/test_post', methods=['POST'])
@login_required
def test_post():
    try:
        args = request.get_json()
        if not args:
            return make_400('Expected json')
        print(args['domain'])
        print(args['address'])
        return make_ok()
    except Exception as e:
        jsonify({'status': e})


@mod.route('/test_post_2', methods=['POST'])
@login_required
def test_post_2():
    try:
        print(request.form['address'])
        return make_ok()
    except Exception as e:
        jsonify({'status': e})


@mod.route('/test_post_3', methods=['POST'])
@login_required
def test_post_3():
    try:
        args = request.get_json()
        print(len(args['test']))
        print(args['test'])
        return make_ok()
    except Exception as e:
        return make_400(e)
