from .. import cfg
import ldap


def check_auth(login, password):
    """
        login is a string with following format userame@example.com
        password is a sting with password
    """
    server = f"ldap://{cfg.AD_SERVER_ADDR}"

    connection = ldap.initialize(server)
    connection.set_option(ldap, OPT_REFERRALS, 0)
    connection.protocol_version = 3
    try:
        connection.simple_bind_s(login, password)
        return True
    except Exception as e:
        return False
