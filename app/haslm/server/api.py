from .. import cfg
from ..db import *
from . import auth
from .exceptions import NotJsonError, NoData
from sqlalchemy.exc import IntegrityError
from sqlalchemy import desc
from . import executor
from . import util
from datetime import datetime
import requests
import logging
import os


def get_user(session, login):
    return session.query(User).filter(
            User.login == login,
            User.status == 'active'
    ).one_or_none()


def get_users(session, logins):
    users = session.query(User).filter(
            User.login.in_(logins)
    ).all()
    if len(logins) != len(users):
        unknow_logins = set(logins) - {user.login for user in users}
        raise NoData('Unknown users: [{}]'.format(', '.join(unknow_logins)))
    return users


def update_logins(login):
    with get_session() as s:
        user = s.query(User).filter(
                User.login == login
        ).first()
        setattr(user, 'logins', user.logins+1)
        s.commit()


def get_users_on_machine(address):
    result = {}
    with get_session() as s:
        machine_users = s.query(User, Access, Machine).filter(
                User.id == Access.vmuser_id,
                Access.machine_id == Machine.id,
                Machine.address == address,
                Access.status == 'active',
        ).all()
        for user, access, _ in machine_users:
            result[user.id] = {
                'login': user.login,
                'name': user.name,
                'surname': user.surname,
                'admin': user.admin,
            }
    return result


def get_machines(session, hosts):
    machines = session.query(Machine).filter(
            Machine.address.in_(hosts),
            Machine.status == 'active'
    ).all()
    if len(hosts) != len(machines):
        unknow_hosts = set(hosts) - {machine.address for machine in machines}
        raise NoData('Unknown hosts: [{}]'.format(', '.join(unknow_hosts)))
    return machines


def get_all_machines():
    result = {}
    with get_session() as s:
        machines = s.query(Machine).filter(
                Machine.status == 'active'
        ).all()
        for machine in machines:
            result[machine.id] = {
                'address': machine.address,
                'domain': machine.domain,
            }
    return result


def get_all_users():
    result = {}
    with get_session() as s:
        users = s.query(User).filter(
                User.status == 'active'
        ).all()
        for user in users:
            result[user.id] = {
                'login': user.login,
                'name': user.name,
                'surname': user.surname,
                'admin': user.admin,
            }
    return result


def get_users_machines(login):
    result = {}
    with get_session() as s:
        user_machines = s.query(User, Access, Machine).filter(
                User.id == Access.vmuser_id,
                Access.machine_id == Machine.id,
                User.login == login,
                Access.status == 'active',
                Machine.status == 'active'
        ).all()
        for _, access, machine in user_machines:
            result[machine.id] = {
                'address': machine.address,
                'domain': machine.domain,
                'access_issued': access.issued,
            }
    return result


def register_user(executor, login, name, surname, is_admin=False):
    with get_session() as s:
        logging.info('Registering new user [{}]'.format(login))
        user = s.query(User).filter(
                User.login == login,
                User.status == 'deleted',
        ).one_or_none()
        save_log(executor, 'register', login)
        if user:
            user.status = 'active'
            user.admin = is_admin
        else:
            user = User(login=login, name=name,
                        surname=surname, admin=is_admin)
            s.add(user)
        if is_admin:
            s.commit()
            machines = s.query(Machine).filter(
                    Machine.status == 'active'
            ).all()
            machines_list = []
            for machine in machines:
                machines_list.append(machine.address)
            grant_access('system', [login], machines_list)


def delete_user(executor, login):
    with get_session() as s:
        user = get_user(s, login)
        if not user:
            raise NoData
        logging.info('Deleting user [{}]'.format(login))
        save_log(executor, 'delete', login)
        user.status = 'deleted'
        keys = s.query(PublicKey).filter(
                PublicKey.vmuser_id == user.id
        ).all()
        for key in keys:
            key.status = 'deleted'
        accesses = s.query(Access).filter(
                Access.vmuser_id == user.id
        ).all()
        for access in accesses:
            access.status = 'deleted'
            access.disabled = datetime.utcnow()


def upload_public_key(login, key, description):
    with get_session() as s:
        user = get_user(s, login)
        if not user:
            raise NoData
        public_key = s.query(PublicKey).filter(
                PublicKey.body == key,
                PublicKey.status == 'deleted'
        ).one_or_none()
        if public_key:
            public_key.status = 'active'
            public_key.description = description
            public_key.update_time = datetime.utcnow()
        else:
            new_public_key = PublicKey(vmuser_id=user.id, body=key,
                               description=description)
            s.add(new_public_key)
        logging.info('Uploading new public key for user [{}]'.format(login))
        save_log(login, 'add ssh-key', description)


def delete_public_key(executor, login, id):
    with get_session() as s:
        public_key = s.query(PublicKey).filter(
                PublicKey.id == id,
                PublicKey.status == 'active'
        ).one_or_none()
        if not public_key:
            raise NoData
        logging.info('Deleting public key for user [{}]'.format(login))
        save_log(executor, "remove ssh-key",
                 login + "'s " + public_key.description)
        public_key.status = 'deleted'


def user_keys(login):
    result = {}
    with get_session() as s:
        keys = s.query(User, PublicKey).filter(
                User.login == login,
                User.status == 'active',
                PublicKey.vmuser_id == User.id,
                PublicKey.status == 'active'
        ).all()
        for _, key in keys:
            result[key.id] = {
                'id': key.id,
                'key': key.body,
                'description': key.description,
                'update_time': key.update_time,
            }
    return result


def new_machine(login, addr, domain):
    with get_session() as s:
        logging.info('Adding machine [{}]'.format(domain))
        save_log(login, 'create', domain + '(' + addr + ')')
        machine = s.query(Machine).filter(
                Machine.address == addr,
                Machine.status == 'deleted',
        ).one_or_none()
        if machine:
            machine.status = 'active'
            machine.domain = domain
        else:
            machine = Machine(address=addr, domain=domain)
            s.add(machine)
        s.commit()
        admins = s.query(User).filter(
                User.admin == True
        ).all()
        admins_list = []
        for admin in admins:
            admins_list.append(admin.login)
        grant_access('system', admins_list, [addr])


def web_terminal(login, address):
    if not access_exist(login, address):
        raise NoData()
    temp_password = util.random_string_digits()
    with get_session() as s:
        machines = get_machines(s, [address])
        script = get_script(s, 'set_password')
    executor.execute(machines, script, env={
        "LOGIN": login,
        "PASSWORD": temp_password
    })
    return {
        "url": "http://{}:7080".format(address),
        "password": temp_password,
    }


def delete_machine(executor, address):
    with get_session() as s:
        machine = s.query(Machine).filter(
                Machine.address == address,
                Machine.status == 'active'
        ).one_or_none()
        if not machine:
            raise NoData
        logging.info('Deleting machine [{}]'.format(address))
        save_log(executor, 'delete',
                 machine.domain + '(' + machine.address + ')')
        machine.status = 'deleted'
        accesses = s.query(Access).filter(
                Access.machine_id == machine.id
        ).all()
        for access in accesses:
            access.status = 'deleted'
            access.disabled = datetime.utcnow()


def host_name(address):
    with get_session() as s:
        return s.query(Machine).filter(Machine.address == address,
                Machine.status == 'active'
        ).first().domain


def grant_access(executor, logins, hosts):
    with get_session() as s:
        users = get_users(s, logins)
        machines = get_machines(s, hosts)
        users_string = ''
        machines_string = ''
        for user in users:
            users_string = users_string + user.login + ' '
            machines_string = ''
            for machine in machines:
                machines_string += f"{machine.domain} ({machine.address}) "
                access = s.query(Access).filter(
                        Access.vmuser_id == user.id,
                        Access.machine_id == machine.id,
                        Access.status == 'active'
                ).one_or_none()
                if not access:
                    access = Access(vmuser_id=user.id, machine_id=machine.id)
                    s.add(access)
        save_log(executor, 'add access',
                 users_string + ' --> ' + machines_string)


def revoke_access(executor2, logins, hosts):
    with get_session() as s:
        users = get_users(s, logins)
        machines = get_machines(s, hosts)
        users_string = ''
        machines_string = ''
        for user in users:
            users_string += user.login + ' '
            machines_string = ''
            for machine in machines:
                machines_string += f"{machine.domain} ({machine.address}) "
                access = s.query(Access).filter(
                    Access.vmuser_id == user.id,
                    Access.machine_id == machine.id,
                    Access.status == 'active'
                ).one_or_none()
                if access:
                    access.status = 'deleted'
                    access.disabled = datetime.utcnow()
#            executor.execute(machines, get_script(s, 'del_user'),
#                env={"LOGIN": user.login.replace('.', '_').replace('@', '_')})
        save_log(executor2, 'remove access',
                 users_string + ' -X- ' + machines_string)


def user_exist(name):
    with get_session() as s:
        exists = s.query(User).filter(
                User.login == name,
                User.status == 'active'
        ).count()
    return True if exists > 0 else False


def machine_exist(address):
    with get_session() as s:
        exist = s.query(Machine).filter(
                Machine.address == address,
                Machine.status == 'active'
        ).count()
    return True if exist > 0 else False


def access_exist(login, address):
    with get_session() as s:
        exist = s.query(User, Access, Machine).filter(
                User.id == Access.vmuser_id,
                Access.machine_id == Machine.id,
                User.login == login,
                Machine.address == address,
                Access.status == 'active',
                Machine.status == 'active',
                User.status == 'active'
        ).count()
    return True if exist > 0 else False


def save_log(executor, action, description):
    with get_session() as s:
        act = Logs(executor=executor, action=action, description=description)
        s.add(act)


def get_logs():
    result = {}
    with get_session() as s:
        num = s.query(Logs).count()
        logs = s.query(Logs).order_by(desc(Logs.id)).all()
        for log in logs:
            result[log.id] = {
                'executor': log.executor,
                'action': log.action,
                'description': log.description,
                'update_time': log.update_time,
            }
    return result


def get_script(s, name):
    script = s.query(Script).get(name)
    if not script:
        raise NoData()
    return script
