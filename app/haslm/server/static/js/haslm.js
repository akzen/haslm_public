function getNewToken() {
    $.get("api/issue_token", function(data) {
        document.getElementById('token-text').innerHTML = data['token']
    });
}

function getWebTerminal(address) {
    $.ajax({
        url: "/api/web_terminal",
        type: "POST",
        data: JSON.stringify({"address": address}),
        contentType: "application/json",
        dataType: "json",
        success: function(data){
            alert("Use current password: " + data["password"]);
            window.location.replace(data["url"]);
        }
    });
}
