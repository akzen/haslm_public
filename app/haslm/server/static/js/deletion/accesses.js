$(document).ready(function(){  
    $(document).on("click", ".delete_a", function(){
        var page = $(this);
        var arr = page.attr('id').split(',');
        $.ajax({
            url: "/api/revoke_access",
            type: "POST",
            data: JSON.stringify({"logins": [arr[0]],
                                  "hosts": [arr[1]]
                                }),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                if(data['status'] == 'ok') {
                    page.parents("tr").remove();
                }
            }
        });
    });
});