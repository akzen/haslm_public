$(document).ready(function(){  
    $(document).on("click", ".delete_k", function(){
        var page = $(this);
        var arr = page.attr('id').split(',');
        $.ajax({
            url: "/api/delete_public_key",
            type: "POST",
            data: JSON.stringify({"login": arr[0],
                                  "key": arr[1]
                                }),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                if(data['status'] == 'ok') {
                    page.parents("tr").remove();
                }
            }
        });
    });
});