$(document).ready(function(){  
    $(document).on("click", ".delete_m", function(){
        var page = $(this);
        $.ajax({
            url: "/api/remove_machine",
            type: "POST",
            data: JSON.stringify({"address": page.attr('id')}),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                if(data['status'] == 'ok') {
                    page.parents("tr").remove();
                }
            }
        });
    });
});