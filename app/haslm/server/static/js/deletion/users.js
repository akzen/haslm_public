$(document).ready(function(){  
    $(document).on("click", ".delete_u", function(){
        var page = $(this);
        $.ajax({
            url: "/api/delete_user",
            type: "POST",
            data: JSON.stringify({"login": page.attr('id')}),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                if(data['status'] == 'ok') {
                    page.parents("tr").remove();
                }
            }
        });
    });
});