$(document).ready(function(){  
    $(document).on("click", ".ssh_copy", function(){
        var $body = document.getElementsByTagName('body')[0];
        var copyText = $(this).attr('id');
        var $tempInput = document.createElement('INPUT');
        $body.appendChild($tempInput);
        $tempInput.setAttribute('value', copyText)
        $tempInput.select();
        document.execCommand('copy');
        $body.removeChild($tempInput);
        alert("Copied SSH link to clipboard");
    });
});