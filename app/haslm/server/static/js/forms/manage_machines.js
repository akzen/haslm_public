$(function() {
    $("#f_create").submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: "/api/create_machine",
            type: "POST",
            data: JSON.stringify({"domain": $(".create_machine_domain").val(),
                                  "address": $(".create_machine_host").val()
                                }),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                $('#msg_create').html(data["description"]);
                $('#msg_remove').html("");
            },
            error: function(data){
                $('#msg_create').html(data.responseJSON['error']);
                $('#msg_remove').html("");
            }
        });
        $("#f_create").trigger('reset');
    });

    $("#f_remove").submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: "/api/remove_machine",
            type: "POST",
            data: JSON.stringify({"address": $(".remove_machine_host").val()}),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                $('#msg_remove').html(data["description"]);
                $('#msg_create').html("");
            },
            error: function(data){
                $('#msg_remove').html(data.responseJSON['error']);
                $('#msg_create').html("");
            }
        });
        $("#f_remove").trigger('reset');
    });
});