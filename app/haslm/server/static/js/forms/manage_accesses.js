$(function() {
    $("#f_grant").submit(function(e) {
        e.preventDefault();
        var logins = $(".grant_access_logins").val().replace(" ", "").split(',');
        var hosts = $(".grant_access_hosts").val().replace(" ", "").split(',');
        $.ajax({
            url: "/api/grant_access",
            type: "POST",
            data: JSON.stringify({"logins": logins,
                                  "hosts": hosts
                                }),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                $('#msg_grant').html(data["description"]);
                $('#msg_revoke').html("");
            },
            error: function(data){
                $('#msg_grant').html(data.responseJSON['error']);
                $('#msg_revoke').html("");
            }
        });
        $("#f_grant").trigger('reset');
    });

    $("#f_revoke").submit(function(e) {
        e.preventDefault();
        var logins = $(".revoke_access_logins").val().replace(" ", "").split(',');
        var hosts = $(".revoke_access_hosts").val().replace(" ", "").split(',');
        $.ajax({
            url: "/api/revoke_access",
            type: "POST",
            data: JSON.stringify({"logins": logins,
                                  "hosts": hosts
                                }),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                $('#msg_revoke').html(data["description"]);
                $('#msg_grant').html("");
            },
            error: function(data){
                $('#msg_revoke').html(data.responseJSON['error']);
                $('#msg_grant').html("");
            }
        });
        $("#f_revoke").trigger('reset');
    });
});