$(function() {
    $("#f_register").submit(function(e) {
        e.preventDefault();
        var is_admin = "False";
        if($(".register_user_admin").is(':checked')) {
            is_admin = "True";
        }
        $.ajax({
            url: "/api/register_user",
            type: "POST",
            data: JSON.stringify({"login": $(".register_user_login").val(),
                                  "name": $(".register_user_name").val(),
                                  "surname": $(".register_user_surname").val(),
                                  "admin": is_admin
                                }),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                $('#msg_register').html(data["description"]);
                $('#msg_delete').html("");
            },
            error: function(data){
                $('#msg_register').html(data.responseJSON['error']);
                $('#msg_delete').html("");
            }
        });
        $("#f_register").trigger('reset');
    });

    $("#f_delete").submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: "/api/delete_user",
            type: "POST",
            data: JSON.stringify({"login": $(".delete_user_surname").val()}),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                $('#msg_delete').html(data["description"]);
                $('#msg_register').html("");
            },
            error: function(data){
                $('#msg_delete').html(data.responseJSON['error']);
                $('#msg_register').html("");
            }
        });
        $("#f_delete").trigger('reset');
    });
});