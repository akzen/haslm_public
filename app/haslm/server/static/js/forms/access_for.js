$(function() {
    $("#f_grant_user").submit(function(e) {
        e.preventDefault();
        var login = $(".c_title")[0].innerHTML.replace('\n', '').replace('\t', '');
        var hosts = $(".grant_user_hosts").val().replace(" ", "").split(',');
        $.ajax({
            url: "/api/grant_access",
            type: "POST",
            data: JSON.stringify({"logins": [login],
                                  "hosts": hosts
                                }),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                $('#msg_grant_user').html(data["description"]);
                location.reload(true);
            },
            error: function(data){
                $('#msg_grant_user').html(data.responseJSON['error']);
            }
        });
        $("#f_grant_user").trigger('reset');
    });

    $("#f_grant_machine").submit(function(e) {
        e.preventDefault();
        var logins = $(".grant_machine_logins").val().replace(" ", "").split(',');
        var host = $(".c_title")[0].innerHTML.replace('\n', '').replace('\t', '').split('(')[1].replace(')', '');
        $.ajax({
            url: "/api/grant_access",
            type: "POST",
            data: JSON.stringify({"logins": logins,
                                  "hosts": [host]
                                }),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                $('#msg_grant_machine').html(data["description"]);
                location.reload(true);
            },
            error: function(data){
                $('#msg_grant_machine').html(data.responseJSON['error']);
            }
        });
        $("#f_grant_machine").trigger('reset');
    });
});