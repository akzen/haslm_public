$(function() {
    $("#f_key").submit(function(e) {
        e.preventDefault();
        $.ajax({
            url: "/api/upload_public_key",
            type: "POST",
            data: JSON.stringify({"key": $(".key_key").val(),
                                  "description": $(".key_description").val()
                                }),
            contentType: "application/json",
            dataType: "json",
            success: function(data){
                $('#msg_key').html(data["description"]);
                location.reload(true);
            },
            error: function(data){
                $('#msg_key').html(data.responseJSON['error']);
            }
        });
        $("#f_key").trigger('reset');
    });
});