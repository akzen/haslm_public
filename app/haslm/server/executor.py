from .. import cfg
from ..db import get_session, ExecutorLog
import uuid
import logging
from concurrent.futures import ThreadPoolExecutor, as_completed
import requests
import json
import os


def _execute_on_host(host, script, shell='bash',
                     env=None, port=cfg.AGENT_PORT):
    url = 'https://{}:{}/execute'.format(host, port)
    try:
        r = requests.post(url, cert=cfg.CERT, verify=False, json={
            'shell': shell,
            'script': script,
            'env': env,
        })
    except requests.exceptions.RequestException as e:
        return {
            'code': 0,
            'stderr': str(e),
            'stdout': 'Request failed',
            'script': script,
        }
    return r.json()


def execute(machines, script, env=None, port=cfg.AGENT_PORT):
    """
        machines is a list of db.Machine instances
    """
    result = {}
    with ThreadPoolExecutor(max_workers=cfg.EXECUTOR_WORKERS) as pool_executor:
        tasks = {}
        for machine in machines:
            task = pool_executor.submit(
                _execute_on_host,
                machine.address,
                script.body,
                script.shell,
                env
            )
            tasks[task] = machine.id
        for task in as_completed(tasks):
            mid = tasks[task]
            result[mid] = task.result()
    return write_executor_log(result, env)


def write_executor_log(result, env):
    eid = str(uuid.uuid4())
    with get_session() as s:
        for mid, ans in result.items():
            log = ExecutorLog(
                execute_id=eid,
                machine_id=mid,
                script=ans['script'],
                env=json_dump(env),
                code=ans['code'],
                stdout=ans['stdout'],
                stderr=ans['stderr'],
            )
            s.add(log)
    return eid


def json_dump(data):
    return json.dumps(data, indent=4, ensure_ascii=False)
