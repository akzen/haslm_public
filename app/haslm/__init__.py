from types import SimpleNamespace
import os


cfg = SimpleNamespace()


def _get_db_connection_string():
    db_connection_string = os.getenv('DB_CONNECTION_STRING')
    if db_connection_string:
        return db_connection_string
    return 'postgresql://{PGUSER}:{PGPASSWORD}@{PGHOST}:{PGPORT}/{PGDATABASE}'.format(**os.environ)


cfg.DEBUG = True if os.getenv('DEBUG') else False
cfg.CSRF_ENABLED = False if os.getenv('DISABLE_CSRF') else True
cfg.SECRET_KEY = os.getenv('SECRET_KEY', 'Top Secret Key, do not use in production!!!')
cfg.AUTH_HEADER_NAME = os.getenv('AUTH_HEADER_NAME', 'X-HASLM-Token')
cfg.HOST = os.getenv('HOST_ADDR', '0.0.0.0')
cfg.PORT = int(os.getenv('PORT', '8080'))
cfg.AD_SERVER_ADDR = os.getenv('AD_SERVER_ADDR', 'localhost')
cfg.DB_CONNECTION_STRING = _get_db_connection_string() 
cfg.RUNTIME_FOLDER = os.path.dirname(os.path.abspath(__file__))
cfg.SCPITS_FOLDER = os.getenv('SCRIPT_FOLDER', '{}/haslm/scripts'.format(cfg.RUNTIME_FOLDER))
cfg.AGENT_PORT = int(os.getenv('AGENT_PORT', 9442))
cfg.EXECUTOR_WORKERS = int(os.getenv('EXECUTOR_WORKERS', 4))
cfg.SEND_INTERVAL = int(os.getenv('SEND_INTERVAL', 15))
cfg.CHECK_INTERVAL = int(os.getenv('CHECK_INTERVAL', 15))
cfg.CLEAN_INTERVAL = int(os.getenv('CLEAN_INTERVAL', 15))
cfg.CONTROLLER_LOGS_INTERVAL = int(os.getenv('CONTROLLER_LOGS_INTERVAL', 1800))
cfg.CRT_PATH = os.getenv('CRT_PATH', os.path.join(os.getcwd(), 'ca.crt'))
cfg.KEY_PATH = os.getenv('KEY_PATH', os.path.join(os.getcwd(), 'ca.key'))
cfg.CERT = (cfg.CRT_PATH, cfg.KEY_PATH)
