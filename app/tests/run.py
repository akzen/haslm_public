from testing.postgresql import Postgresql
import pytest
import os


with Postgresql() as postgres:
    os.environ['DB_CONNECTION_STRING'] = postgres.url()
    pytest.main(['-s'])
