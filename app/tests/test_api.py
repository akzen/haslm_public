from base import HasvmTestBase
import haslm.db as db
import haslm.server.api as api


EXECUTOR_ID = 'root@root'
LOGIN = 'test@test'
NAME = 'testname'
SURNAME = 'testsurname'
PKEY = 'SimplePublicKey'
DESC = 'Test key'
MACHINE_ADDR = '127.0.0.2'
MACHINE_DOMAIN = 'local.host'


class TestApi(HasvmTestBase):
    def test_1_register(self):
        api.register_user(EXECUTOR_ID, LOGIN, NAME, SURNAME)
        with db.get_session() as s:
            user = s.query(db.User).filter(
                    db.User.login == LOGIN
            ).one_or_none()
        assert user is not None
        assert user.name == NAME
        assert user.surname == SURNAME

    def test_2_upload_key(self):
        api.upload_public_key(LOGIN, PKEY, DESC)
        with db.get_session() as s:
            user_keys = s.query(db.User, db.PublicKey).filter(
                    db.User.id == db.PublicKey.vmuser_id,
                    db.User.login == LOGIN,
                    db.PublicKey.status == 'active',
            ).all()
        assert len(user_keys) == 1
        for user, key in user_keys:
            assert user.login == LOGIN
            assert key.body == PKEY

    def test_3_get_keys(self):
        _, data = list(api.user_keys(LOGIN).items())[0]
        assert data['key'] == PKEY
        assert data['description'] == DESC

    def test_4_new_machine(self):
        api.new_machine(EXECUTOR_ID, MACHINE_ADDR, MACHINE_DOMAIN)
        with db.get_session() as s:
            machine = s.query(db.Machine).filter(
                    db.Machine.address == MACHINE_ADDR
            ).one_or_none()
        assert machine is not None
        assert machine.domain == MACHINE_DOMAIN

    def test_5_grant(self):
        api.grant_access(EXECUTOR_ID, [LOGIN], [MACHINE_ADDR])
        with db.get_session() as s:
            data = s.query(db.User, db.Access, db.Machine).filter(
                    db.User.login == LOGIN,
                    db.Access.vmuser_id == db.User.id,
                    db.Access.machine_id == db.Machine.id,
                    db.Machine.address == MACHINE_ADDR,
            ).one_or_none()
        assert data is not None
        assert data[1].status == 'active'

    def test_6_users_machines_when_one(self):
        data = api.get_users_machines(LOGIN)
        assert data and len(data) == 1
        accessed_machine = list(data.values())[0]
        assert accessed_machine['address'] == MACHINE_ADDR
        assert accessed_machine['domain'] == MACHINE_DOMAIN

    def test_7_revoke(self):
        api.revoke_access(EXECUTOR_ID, [LOGIN], [MACHINE_ADDR])
        with db.get_session() as s:
            data = s.query(db.User, db.Access, db.Machine).filter(
                    db.User.login == LOGIN,
                    db.Access.vmuser_id == db.User.id,
                    db.Access.machine_id == db.Machine.id,
                    db.Machine.address == MACHINE_ADDR,
            ).one_or_none()
        assert data is not None
        assert data[1].status == 'deleted'

    def test_8_users_machines_when_nothin(self):
        data = api.get_users_machines(LOGIN)
        assert len(data) == 0

    def test_9_grant_again(self):
        api.grant_access(EXECUTOR_ID, [LOGIN], [MACHINE_ADDR])
        with db.get_session() as s:
            data = s.query(db.User, db.Access, db.Machine).filter(
                    db.User.login == LOGIN,
                    db.Access.vmuser_id == db.User.id,
                    db.Access.machine_id == db.Machine.id,
                    db.Access.status == 'active',
                    db.Machine.address == MACHINE_ADDR,
            ).one_or_none()
        assert data is not None
        assert data[1].status == 'active'

    def test_10_delete_key(self):
        api.delete_public_key(EXECUTOR_ID, LOGIN, 1)
        with db.get_session() as s:
            user_keys = s.query(db.User, db.PublicKey).filter(
                    db.User.id == db.PublicKey.vmuser_id,
                    db.User.login == LOGIN,
                    db.PublicKey.status == 'deleted',
            ).all()
        for user, key in user_keys:
            assert user.login == LOGIN
            assert key.body == PKEY

    def test_11_delete_user(self):
        api.delete_user(EXECUTOR_ID, LOGIN)
        with db.get_session() as s:
            user = s.query(db.User).filter(
                    db.User.login == LOGIN
            ).one_or_none()
        assert user.status == 'deleted'

    def test_12_register_user_again(self):
        api.register_user(EXECUTOR_ID, LOGIN, NAME, SURNAME)
        with db.get_session() as s:
            user = s.query(db.User).filter(
                    db.User.login == LOGIN
            ).one_or_none()
        assert user is not None
        assert user.name == NAME
        assert user.surname == SURNAME
        assert user.status == 'active'
