#!/bin/bash


echo "Copying files"
mkdir /var/www/haslm
cp -r ../app /var/www/haslm/app  # application
cp ./nginx.conf /etc/nginx/nginx.conf  # nginx cfg
cp -r ./ssl  /etc/nginx/ssl  # ssl certs (DO NOT USE THIS ON PRODUCTION)
cp ./haslm.service /etc/systemd/system/haslm.service  # systemd unit for application

echo "Installing dependencies"
apt install python3-pip &&
pip3 install virtualenv &&
cd /var/www/haslm/app &&
python3 -m virtualenv venv &&
source ./venv/bin/activate &&
pip install -r requirements.txt

echo "Done!"
