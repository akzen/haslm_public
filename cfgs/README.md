# Как деплоить

Для деплоя сервера в первую очередь нужно склонировать весь репозиторий.
Потом зайти в эту папочку и запускить от рута скрипт `./deploy.sh`.
Скрипт скорирует конфиг nginx в /etc/nginx, код сервера в /var/www/haslm и создаст systemd unit haslm.service.
Если скрипт отработал успешно, бэкенд можно запустить командами 
            `sudo systemctl restart nginx`
            `sudo systemctl restart haslm`