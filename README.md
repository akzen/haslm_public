# HSE Authorization Service for LINUX Machines Backend
Это репозиторий с кодом бекенд сервиса, который предоставляет механизм авторизации пользователей на удалённых машинах.
В его задачи входит:

*	Хранить публичные ключи пользователей
*	Предоставять API для автоматизации процесса выдачи доступов
*	Непосредственно ходить на виртуальные машины, создавать там пользователей и разностить ключи

## Структура системы:
![Картинка](https://bitbucket.org/akzen/haslm_public/raw/61b760df255e4824ce6949e6e10522d4ebea2a1e/doc/HASLM_scheme.png)

## Структура репозитория:
*	В app лежит код самого бекенда
*	В extra всякие полезные скрипты, но которые прямого отношения к коду бекенда не имеют
*	В doc документация и картинки

## Используемые технологии:
*	Основа - [Python3](https://www.python.org/)
*	СУБД - [PostgreSQL](https://www.postgresql.org/)
*	Связь с БД - [SQLAlchemy](https://www.sqlalchemy.org/)
*	Web-framework - [Flask](http://flask.pocoo.org/)
*	Балансер - [Nginx](https://nginx.org/)
*	Web-интерфейс - [Bootstrap 4.3.1](https://getbootstrap.com/)

## Установка:	
*	В первую очередь ставим нужные пакеты (Ubuntu):
		
		sudo apt install git python python3 python3-pip
		pip3 install virtualenv

*	Клонинуем репозиторий и настраиваем виртуальное окружение (bash/zsh):

		git clone git@bitbucket.org:mirdim/backend.git
		cd app
		python3 -m virtualenv venv
		. venv/bin/activate
	
*	Ставим зависимости проекта:

		pip3 install -r requirements.txt

*   Настраиваем PostgreSQL:
    *   Сервис берёт настройки БД через перменные окружения, поэтому рекомендуется использовать специальный скрипт (например [такой](https://bitbucket.org/akzen/haslm_public/src/master/extra/setup.sh)) и инициилизировать переменные окручения через `. setup.sh`
    *   Так же можно поднять PostgreSQL в облаке (например [Heroku](https://www.heroku.com/))

*   При первом запуске необходимо:
	*	указать опцию `--create-tables`, чтобы в БД создались нужные таблички
	*	Указать логин первичной администраторской учётной записи

*   Запуск происходит через `python3 main.py server`


## Дополнительная информация по компонентам и их настройке:
*   [База данных](https://bitbucket.org/akzen/haslm_public/src/master/doc/db.md)
*   [Контроллер](https://bitbucket.org/akzen/haslm_public/src/master/doc/controller.md)


## Информация  взаимодействию с системой:
*	[WEB-интерфейс]()
*	[HTTP API](https://bitbucket.org/akzen/haslm_public/src/master/doc/http_api.md)
